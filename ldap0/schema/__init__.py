# -*- coding: utf-8 -*-
"""
ldap0.schema -  LDAPv3 schema handling
"""

from .subentry import SCHEMA_ATTRS, SCHEMA_CLASS_MAPPING, SCHEMA_ATTR_MAPPING, SubSchema
